import * as os from 'os';
import * as fs from 'fs';
import * as path from 'path';

//import { app } from '@sgtools/application';
import { Application } from '@sgtools/application';

export class Config
{
    private path: string;
    private data: Object;
    private raw : boolean;
    private app : Application;

    constructor()
    {
        //this.getPath();
        this.data = {};
        this.raw  = false;
    }

    public setRaw()
    {
        this.raw = true;
    }

    public getRaw(): string
    {
        return this.raw ? this.data['raw'] : null;
    }

    public getPath(): string
    {
        // TODO CLEAN FILE TO REMOVE / : @ etc.
        this.path = path.join(os.homedir(), '.' + this.app.name.split('/').join('_').split('@').join('') + 'rc');
        return this.path;
    }

    private setItem(key: string, value: any, group?: string)
    {
        if (typeof group === 'undefined' || group === '') {
            this.data[key] = value;
        } else {
            if (!Object.keys(this.data).includes(group)) this.data[group] = {};
            this.data[group][key] = value;
        }
        this.save();
    }

    public setString(key: string, value: string, group?: string)
    {
        this.setItem(key, value, group);
    }

    public setBoolean(key: string, value: boolean, group?: string)
    {
        this.setItem(key, value ? 'true': 'false', group);
    }

    public setNumber(key: string, value: number, group?: string)
    {
        this.setItem(key, value.toString(), group);
    }

    private getItem(key: string, group?: string): string
    {
        if (typeof group === 'undefined' || group === '') {
            if (!Object.keys(this.data).includes(key)) return null;
            return this.data[key];
        } else {
            if (!Object.keys(this.data).includes(group)) return null;
            if (typeof this.data[group] !== 'object') return null;
            if (!Object.keys(this.data[group]).includes(key)) return null;
            return this.data[group][key];
        }
    }

    public getString(key: string, group?: string): string
    {
        return this.getItem(key, group);
    }

    public getBoolean(key: string, group?: string): boolean
    {
        return this.getItem(key, group) === 'true';
    }

    public getInt(key: string, group?: string): number
    {
        return parseInt(this.getItem(key, group));
    }

    public getFloat(key: string, group?: string): number
    {
        return parseFloat(this.getItem(key, group));
    }

    public getGroup(group: string): Object
    {
        if (!Object.keys(this.data).includes(group)) return null;
        if (typeof this.data[group] !== 'object') return null;

        return this.data[group];
    }

    public getGroups(): string[]
    {
        let groups = [];
        Object.keys(this.data).forEach(key => {
            if (typeof this.data[key] === 'object') {
                groups.push(key);
            }
        });
        return groups;
    }

    public filterGroups(struct: string[]): string[]
    {
        return this.getGroups().filter(group => {
            return this.checkGroupStructure(group, struct);
        });
    }

    private checkGroupStructure(group: string, struct: string[]): boolean
    {
        if (!Object.keys(this.data).includes(group)) return false;
        if (typeof this.data[group] !== 'object') return false;
   
        let isOK = true;
        struct.forEach(item => {
            if (this.getItem(item, group) === null) isOK = false;
        });

        return isOK;
    }

    public load(app: Application)
    {
        this.app = app;

        if (app.name === '') return false;
        this.getPath();

        if (fs.existsSync(this.path)) {
            let rawData = fs.readFileSync(this.path, 'utf8');

            if (this.raw) {
                this.data['raw'] = rawData;
                return true;
            }

            let inGroup = false;
            let group   = '';
            rawData.split('\n').forEach(line => {
                if (inGroup && line.trim() === `[/${group}]`) {
                    inGroup = false;
                } else if (/\[(.+)\]/.test(line.trim())) {
                    inGroup = true;
                    group = /\[(.+)\]/.exec(line.trim())[1];
                    if (!this.data[group]) this.data[group] = {};
                } else {
                    let items = line.split(/=(.*)/);
                    let key   = items[0].trim();
                    if (key !== '') {
                        let value = (items.length > 1 ? items[1].trim() : '');
                        if (inGroup) {
                            this.data[group][key] = value;
                        } else {
                            this.data[key] = value;
                        }
                    }
                }
            });
            return true;
        }
        return false;
    }

    public save()
    {
        if (this.app.name === '') return false;
        this.path = path.join(os.homedir(), '.' + this.app.name);

        let rawData: string = '';

        Object.keys(this.data).forEach(key => {
            rawData += (rawData === '' ? '' : '\n');
            if (typeof this.data[key] === 'object') {
                rawData += `[${key}]\n`;
                Object.keys(this.data[key]).forEach(sKey => {
                    rawData += `${sKey}=${this.data[key][sKey]}\n`;
                });
                rawData += `[/${key}]`;
            } else {
                rawData += key + '=' + this.data[key];
            }
        });

        fs.writeFileSync(this.path, rawData);

        return true;
    }
}

export const config = new Config();
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = exports.Config = void 0;
const os = require("os");
const fs = require("fs");
const path = require("path");
class Config {
    constructor() {
        //this.getPath();
        this.data = {};
        this.raw = false;
    }
    setRaw() {
        this.raw = true;
    }
    getRaw() {
        return this.raw ? this.data['raw'] : null;
    }
    getPath() {
        // TODO CLEAN FILE TO REMOVE / : @ etc.
        this.path = path.join(os.homedir(), '.' + this.app.name.split('/').join('_').split('@').join('') + 'rc');
        return this.path;
    }
    setItem(key, value, group) {
        if (typeof group === 'undefined' || group === '') {
            this.data[key] = value;
        }
        else {
            if (!Object.keys(this.data).includes(group))
                this.data[group] = {};
            this.data[group][key] = value;
        }
        this.save();
    }
    setString(key, value, group) {
        this.setItem(key, value, group);
    }
    setBoolean(key, value, group) {
        this.setItem(key, value ? 'true' : 'false', group);
    }
    setNumber(key, value, group) {
        this.setItem(key, value.toString(), group);
    }
    getItem(key, group) {
        if (typeof group === 'undefined' || group === '') {
            if (!Object.keys(this.data).includes(key))
                return null;
            return this.data[key];
        }
        else {
            if (!Object.keys(this.data).includes(group))
                return null;
            if (typeof this.data[group] !== 'object')
                return null;
            if (!Object.keys(this.data[group]).includes(key))
                return null;
            return this.data[group][key];
        }
    }
    getString(key, group) {
        return this.getItem(key, group);
    }
    getBoolean(key, group) {
        return this.getItem(key, group) === 'true';
    }
    getInt(key, group) {
        return parseInt(this.getItem(key, group));
    }
    getFloat(key, group) {
        return parseFloat(this.getItem(key, group));
    }
    getGroup(group) {
        if (!Object.keys(this.data).includes(group))
            return null;
        if (typeof this.data[group] !== 'object')
            return null;
        return this.data[group];
    }
    getGroups() {
        let groups = [];
        Object.keys(this.data).forEach(key => {
            if (typeof this.data[key] === 'object') {
                groups.push(key);
            }
        });
        return groups;
    }
    filterGroups(struct) {
        return this.getGroups().filter(group => {
            return this.checkGroupStructure(group, struct);
        });
    }
    checkGroupStructure(group, struct) {
        if (!Object.keys(this.data).includes(group))
            return false;
        if (typeof this.data[group] !== 'object')
            return false;
        let isOK = true;
        struct.forEach(item => {
            if (this.getItem(item, group) === null)
                isOK = false;
        });
        return isOK;
    }
    load(app) {
        this.app = app;
        if (app.name === '')
            return false;
        this.getPath();
        if (fs.existsSync(this.path)) {
            let rawData = fs.readFileSync(this.path, 'utf8');
            if (this.raw) {
                this.data['raw'] = rawData;
                return true;
            }
            let inGroup = false;
            let group = '';
            rawData.split('\n').forEach(line => {
                if (inGroup && line.trim() === `[/${group}]`) {
                    inGroup = false;
                }
                else if (/\[(.+)\]/.test(line.trim())) {
                    inGroup = true;
                    group = /\[(.+)\]/.exec(line.trim())[1];
                    if (!this.data[group])
                        this.data[group] = {};
                }
                else {
                    let items = line.split(/=(.*)/);
                    let key = items[0].trim();
                    if (key !== '') {
                        let value = (items.length > 1 ? items[1].trim() : '');
                        if (inGroup) {
                            this.data[group][key] = value;
                        }
                        else {
                            this.data[key] = value;
                        }
                    }
                }
            });
            return true;
        }
        return false;
    }
    save() {
        if (this.app.name === '')
            return false;
        this.path = path.join(os.homedir(), '.' + this.app.name);
        let rawData = '';
        Object.keys(this.data).forEach(key => {
            rawData += (rawData === '' ? '' : '\n');
            if (typeof this.data[key] === 'object') {
                rawData += `[${key}]\n`;
                Object.keys(this.data[key]).forEach(sKey => {
                    rawData += `${sKey}=${this.data[key][sKey]}\n`;
                });
                rawData += `[/${key}]`;
            }
            else {
                rawData += key + '=' + this.data[key];
            }
        });
        fs.writeFileSync(this.path, rawData);
        return true;
    }
}
exports.Config = Config;
exports.config = new Config();

import { Application } from '@sgtools/application';
export declare class Config {
    private path;
    private data;
    private raw;
    private app;
    constructor();
    setRaw(): void;
    getRaw(): string;
    getPath(): string;
    private setItem;
    setString(key: string, value: string, group?: string): void;
    setBoolean(key: string, value: boolean, group?: string): void;
    setNumber(key: string, value: number, group?: string): void;
    private getItem;
    getString(key: string, group?: string): string;
    getBoolean(key: string, group?: string): boolean;
    getInt(key: string, group?: string): number;
    getFloat(key: string, group?: string): number;
    getGroup(group: string): Object;
    getGroups(): string[];
    filterGroups(struct: string[]): string[];
    private checkGroupStructure;
    load(app: Application): boolean;
    save(): boolean;
}
export declare const config: Config;

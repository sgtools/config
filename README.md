# Welcome to @sgtools/config
[![Version](https://npm.bmel.fr/-/badge/sgtools/config.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/config)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/config/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Application framework nodejs applications
> Configuration file


## Install

```sh
npm install @sgtools/config
```

## Usage

```sh
import { config } from '@sgtools/config';
```

Code documentation can be found [here](https://gitlab.com/sgtools/config/-/blob/master/docs/README.md).


## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>


## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/config/issues). You can also contact the author.


## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.

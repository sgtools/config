[@sgtools/config](../README.md) / [Exports](../modules.md) / Config

# Class: Config

## Hierarchy

* **Config**

## Table of contents

### Constructors

- [constructor](config.md#constructor)

### Properties

- [app](config.md#app)
- [data](config.md#data)
- [path](config.md#path)
- [raw](config.md#raw)

### Methods

- [checkGroupStructure](config.md#checkgroupstructure)
- [filterGroups](config.md#filtergroups)
- [getBoolean](config.md#getboolean)
- [getFloat](config.md#getfloat)
- [getGroup](config.md#getgroup)
- [getGroups](config.md#getgroups)
- [getInt](config.md#getint)
- [getItem](config.md#getitem)
- [getPath](config.md#getpath)
- [getRaw](config.md#getraw)
- [getString](config.md#getstring)
- [load](config.md#load)
- [save](config.md#save)
- [setBoolean](config.md#setboolean)
- [setItem](config.md#setitem)
- [setNumber](config.md#setnumber)
- [setRaw](config.md#setraw)
- [setString](config.md#setstring)

## Constructors

### constructor

\+ **new Config**(): [*Config*](config.md)

**Returns:** [*Config*](config.md)

Defined in: index.ts:13

## Properties

### app

• `Private` **app**: *Application*

Defined in: index.ts:13

___

### data

• `Private` **data**: Object

Defined in: index.ts:11

___

### path

• `Private` **path**: *string*

Defined in: index.ts:10

___

### raw

• `Private` **raw**: *boolean*

Defined in: index.ts:12

## Methods

### checkGroupStructure

▸ `Private`**checkGroupStructure**(`group`: *string*, `struct`: *string*[]): *boolean*

#### Parameters:

Name | Type |
------ | ------ |
`group` | *string* |
`struct` | *string*[] |

**Returns:** *boolean*

Defined in: index.ts:124

___

### filterGroups

▸ **filterGroups**(`struct`: *string*[]): *string*[]

#### Parameters:

Name | Type |
------ | ------ |
`struct` | *string*[] |

**Returns:** *string*[]

Defined in: index.ts:117

___

### getBoolean

▸ **getBoolean**(`key`: *string*, `group?`: *string*): *boolean*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`group?` | *string* |

**Returns:** *boolean*

Defined in: index.ts:83

___

### getFloat

▸ **getFloat**(`key`: *string*, `group?`: *string*): *number*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`group?` | *string* |

**Returns:** *number*

Defined in: index.ts:93

___

### getGroup

▸ **getGroup**(`group`: *string*): Object

#### Parameters:

Name | Type |
------ | ------ |
`group` | *string* |

**Returns:** Object

Defined in: index.ts:98

___

### getGroups

▸ **getGroups**(): *string*[]

**Returns:** *string*[]

Defined in: index.ts:106

___

### getInt

▸ **getInt**(`key`: *string*, `group?`: *string*): *number*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`group?` | *string* |

**Returns:** *number*

Defined in: index.ts:88

___

### getItem

▸ `Private`**getItem**(`key`: *string*, `group?`: *string*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`group?` | *string* |

**Returns:** *string*

Defined in: index.ts:65

___

### getPath

▸ **getPath**(): *string*

**Returns:** *string*

Defined in: index.ts:32

___

### getRaw

▸ **getRaw**(): *string*

**Returns:** *string*

Defined in: index.ts:27

___

### getString

▸ **getString**(`key`: *string*, `group?`: *string*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`group?` | *string* |

**Returns:** *string*

Defined in: index.ts:78

___

### load

▸ **load**(`app`: *Application*): *boolean*

#### Parameters:

Name | Type |
------ | ------ |
`app` | *Application* |

**Returns:** *boolean*

Defined in: index.ts:137

___

### save

▸ **save**(): *boolean*

**Returns:** *boolean*

Defined in: index.ts:179

___

### setBoolean

▸ **setBoolean**(`key`: *string*, `value`: *boolean*, `group?`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`value` | *boolean* |
`group?` | *string* |

**Returns:** *void*

Defined in: index.ts:55

___

### setItem

▸ `Private`**setItem**(`key`: *string*, `value`: *any*, `group?`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`value` | *any* |
`group?` | *string* |

**Returns:** *void*

Defined in: index.ts:39

___

### setNumber

▸ **setNumber**(`key`: *string*, `value`: *number*, `group?`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`value` | *number* |
`group?` | *string* |

**Returns:** *void*

Defined in: index.ts:60

___

### setRaw

▸ **setRaw**(): *void*

**Returns:** *void*

Defined in: index.ts:22

___

### setString

▸ **setString**(`key`: *string*, `value`: *string*, `group?`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |
`value` | *string* |
`group?` | *string* |

**Returns:** *void*

Defined in: index.ts:50

[@sgtools/config](README.md) / Exports

# @sgtools/config

## Table of contents

### Classes

- [Config](classes/config.md)

### Variables

- [config](modules.md#config)

## Variables

### config

• `Const` **config**: [*Config*](classes/config.md)

Defined in: index.ts:205
